﻿using System;

using Microsoft.AspNetCore.Mvc;

using ProjectStructure.Repositories;
using ProjectStructure.Models;
using ProjectStructure.Interfaces;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private IRepository<Project> repository;

        public ProjectsController()
        {
            repository = RepositoryFacade.ProjectRepository;
        }

        // GET: api/Projects
        [HttpGet]
        public IActionResult GetProjects()
        {
            return Ok(repository.Get());
        }


        //GET: api/Projects/id
        [HttpGet("{id}")]
        public IActionResult GetProject(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(repository.Get(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }

        //POST: api/Projects
        [HttpPost("")]
        public IActionResult PostProject(Project project)
        {
            if (project == null)
            {
                return BadRequest();
            }
            try
            {
                repository.Create(project);
                return Created("api/Projects", project);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        //PUT: api/Projects
        [HttpPut("")]
        public IActionResult PutProject(Project project)
        {
            if (project == null)
            {
                return BadRequest();
            }
            try
            {
                repository.Update(project);
                return Ok(project);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        //DELETE: api/Projects
        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                repository.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}


