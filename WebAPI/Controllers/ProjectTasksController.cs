﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Interfaces;
using ProjectStructure.Models;
using ProjectStructure.Repositories;
using System;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTasksController : ControllerBase
    {
        private IRepository<ProjectTask> projectTaskRepository;
        public ProjectTasksController()
        {
            projectTaskRepository = RepositoryFacade.ProjectTaskRepository;
        }

        // GET api/ProjectTasks
        [HttpGet]
        public IActionResult GetProjectTasks()
        {
            return Ok(projectTaskRepository.Get());
        }

        // GET api/ProjectTasks/id
        [HttpGet("{id}")]
        public IActionResult GetProjectTask(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(projectTaskRepository.Get(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }


        // POST api/ProjectTasks
        [HttpPost]
        public IActionResult Post(ProjectTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }
            try
            {
                projectTaskRepository.Create(task);
                return Created("api/ProjectTasks", task);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // DELETE api/ProjectTasks/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                projectTaskRepository.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // PUT api/ProjectTasks
        [HttpPut()]
        public IActionResult Put(ProjectTask task)
        {
            if (task == null)
            {
                return BadRequest();
            }
            try
            {
                projectTaskRepository.Update(task);
                return Ok(task);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

    }
}
