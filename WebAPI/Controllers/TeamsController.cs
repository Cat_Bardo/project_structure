﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Interfaces;
using ProjectStructure.Models;
using ProjectStructure.Repositories;
using System;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private IRepository<Team> teamRepository;
        public TeamsController()
        {
            teamRepository = RepositoryFacade.TeamRepository;
        }

        // GET api/Teams
        [HttpGet]
        public IActionResult GetTeams()
        {
            return Ok(teamRepository.Get());
        }

        // GET api/Teams/id
        [HttpGet("{id}")]
        public IActionResult GetTeam(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                return Ok(teamRepository.Get(id));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }


        // POST api/Teams
        [HttpPost]
        public IActionResult Post(Team team)
        {
            if(team == null)
            {
                return BadRequest();
            }
            try
            {
                teamRepository.Create(team);
                return Created("api/Teams", team);
            }
            catch(ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // DELETE api/Teams/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            {
                teamRepository.Delete(id);
                return NoContent();
            }
            catch(ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // PUT api/Teams
        [HttpPut()]
        public IActionResult Put(Team team)
        {
            if (team == null)
            {
                return BadRequest();
            }
            try
            {
                teamRepository.Update(team);
                return Ok(team);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
