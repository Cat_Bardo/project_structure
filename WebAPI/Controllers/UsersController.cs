﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Interfaces;
using ProjectStructure.Models;
using ProjectStructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IRepository<User> userRepository;
        public UsersController()
        {
            userRepository = RepositoryFacade.UserRepository;
        }

        // GET api/Users
        [HttpGet]
        public IActionResult GetUsers()
        {
            return Ok(userRepository.Get());
        }

        // GET api/Users/id
        [HttpGet("{id}")]
        public IActionResult GetUsers(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try 
            {
                return Ok(userRepository.Get(id)); 
            }
            catch(ArgumentException e) 
            { 
                return NotFound(e.Message); 
            }
            
        }

        // POST api/Users
        [HttpPost]
        public IActionResult Post(User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            try
            {
                userRepository.Create(user);
                return Created("api/User", user);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // DELETE api/Users/{id}   
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            try
            { 
                userRepository.Delete(id);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

        // PUT api/Users
        [HttpPut()]
        public IActionResult Put(User user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            try
            {
                userRepository.Update(user);
                return Ok(user);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }



    }
}
