﻿using Newtonsoft.Json;
using ProjectStructure.Models;
using ProjectStructure.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI
{
    public static class Seeder
    {
        public static void Seed()
        {
            RepositoryFacade.ProjectRepository = 
                new Repository<Project>(
                    JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("StartData\\Projects.json"))
                    );
            RepositoryFacade.ProjectTaskRepository = 
                new Repository<ProjectTask>(
                    JsonConvert.DeserializeObject<List<ProjectTask>>(File.ReadAllText("StartData\\Tasks.json"))
                    );
            RepositoryFacade.TeamRepository =
                new Repository<Team>(
                    JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("StartData\\Teams.json"))
                    );
            RepositoryFacade.UserRepository =
                new Repository<User>(
                    JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("StartData\\Users.json"))
                    );
        }
        
    }
}
