﻿
using System.Collections.Generic;


namespace UserInterface.ClientDTOs
{
    public class TeamsUsersDTO
    {
        public TeamDTO Team { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}
