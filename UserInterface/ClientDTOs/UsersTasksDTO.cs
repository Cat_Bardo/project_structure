﻿
using System.Collections.Generic;


namespace UserInterface.ClientDTOs
{
    public class UsersTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<ProjectTaskDTO> Tasks { get; set; }
    }
}
