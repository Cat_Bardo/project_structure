﻿

namespace UserInterface.ClientDTOs
{
    public class TaskIdNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
