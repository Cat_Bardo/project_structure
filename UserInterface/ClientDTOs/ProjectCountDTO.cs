﻿

namespace UserInterface.ClientDTOs
{
    public class ProjectCountDTO
    {
        public ProjectDTO Key { get; set; }
        public int Value { get; set; }
    }
}
