﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DTO
{
    public class UserInfoDTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TasksCount { get; set; }
        public int UnfinishedAndCanceledTasksCount { get; set; }
        public ProjectTask LongestTask { get; set; }
    }
}
