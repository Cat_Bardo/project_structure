﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DTO
{
    public class ProjectInfoDTO
    {
        public Project Project { get; set; }
        public ProjectTask LongestTaskByDicription { get; set; }
        public ProjectTask LessTaskByName { get; set; }
        public int UserCounWhereDescriptionMore20orTaskCounLess3 { get; set; }
    }
}
