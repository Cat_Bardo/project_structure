﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DTO
{
    public class TeamsUsersDTO
    {
        public Team Team { get; set; }
        public List<User> Users { get; set; }
    }
}
