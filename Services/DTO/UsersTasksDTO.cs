﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DTO
{
    public class UsersTasksDTO
    {
        public User User { get; set; }
        public IEnumerable<ProjectTask> Tasks { get; set; }
    }
}
