﻿using ProjectStructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.DTO
{
    public class ProjectCountDTO
    {
        public Project Key { get; set; }
        public int Value { get; set; }
    }
}
