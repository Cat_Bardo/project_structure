﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Interfaces;

namespace ProjectStructure.Models
{
    public class Team : IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}
