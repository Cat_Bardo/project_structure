﻿using ProjectStructure.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProjectStructure.Models
{
    public class Project : IEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }



    }
}
