﻿using System;
using System.Linq;
using System.Collections.Generic;

using ProjectStructure.DTO;
using ProjectStructure.Models;
using ProjectStructure.Repositories;

namespace ProjectStructure.Services
{
    public class LINQServices
    {
        public List<ProjectCountDTO> GetCountOfUserTasksInProject(int id)
        {
            if (RepositoryFacade.ProjectRepository.Get().Any(p => p.AuthorId == id))
            {
                return RepositoryFacade.ProjectRepository.Get().Where(p => p.AuthorId == id).Select(p => new ProjectCountDTO
                {
                    Key = p,
                    Value = RepositoryFacade.ProjectTaskRepository.Get().Where(t => t.ProjectId == p.Id).Count()
                }
                ).ToList();
            }
            throw new ArgumentException("Not found projects whis same AuthorId");
        }
        public List<ProjectTask> GetTasksByUserIdWhereNameLess45(int id)
        {
            if (RepositoryFacade.UserRepository.Get().Any(u => u.Id == id))
            {
                if (RepositoryFacade.ProjectTaskRepository.Get().Any(t => t.PerformerId == id))
                    return RepositoryFacade.ProjectTaskRepository.Get().Where(t => t.PerformerId == id && t.Name.Length < 45).ToList();
                throw new ArgumentException("Not found any tasks for user");
            }
            throw new ArgumentException("Not found user");
        }
   
        public List<TaskIdNameDTO> GetFinishedTasksInThisYearByUserId(int id)
        {
            if (RepositoryFacade.UserRepository.Get().Any(u => u.Id == id))
            {
                if (RepositoryFacade.ProjectTaskRepository.Get().Any(t => t.PerformerId == id))
                {
                    return RepositoryFacade.ProjectTaskRepository.Get()
                    .Where(t => t.PerformerId == id
                        && t.State == (int)TaskStates.Finished
                        && t.FinishedAt.Year == DateTime.Now.Year)
                    .Select(t => new TaskIdNameDTO { Id = t.Id, Name = t.Name }).ToList();
                }
                throw new ArgumentException("Not found any tasks for user");
            }
            throw new ArgumentException("Not found user");
        }
        
        public List<TeamsUsersDTO> GetTeamsMembersOverTenYearsOld()
        {
            return RepositoryFacade.TeamRepository.Get()
                .GroupJoin(RepositoryFacade.UserRepository.Get()
                .Where(x => DateTime.Now.Year - x.Birthday.Year > 10)
                .OrderByDescending(x => x.RegisteredAt),
                 t => t.Id,
                 x => x.TeamId,
                 (team, user) => new TeamsUsersDTO { Team = team, Users = user.Select(u => u).ToList() })
                .Where(x => x.Users.Any()).ToList();
        }
       
        public List<UsersTasksDTO> GetSortedUsersWithSortedTasks()
        {
            return RepositoryFacade.UserRepository.Get().GroupJoin(
                RepositoryFacade.ProjectTaskRepository.Get()
                .OrderByDescending(x => x.Name.Length),
                u => u.Id,
                t => t.PerformerId,
                (user, tasks) => new UsersTasksDTO { User = user, Tasks = tasks })
                .OrderBy(x => x.User.FirstName).ToList();
        }

        public UserInfoDTO GetUserInfoById(int id)
        {
            
            if (RepositoryFacade.UserRepository.Get().Any(u => u.Id == id))
            {
                if (RepositoryFacade.ProjectRepository.Get().Any(p => p.AuthorId == id)
                    && RepositoryFacade.ProjectTaskRepository.Get().Any(p => p.PerformerId == id)
                    && RepositoryFacade.UserRepository.Get(id).TeamId != null
                )
                {
                    return RepositoryFacade.UserRepository.Get().Select(user => new UserInfoDTO
                    {
                        User = user,
                        LastProject = RepositoryFacade.ProjectRepository.Get()
                        .Where(p => p.AuthorId == user.Id)
                        .FirstOrDefault(x => x.CreatedAt == RepositoryFacade.ProjectRepository.Get()
                            .Where(p => p.AuthorId == user.Id)
                            .Max(x => x.CreatedAt)),
                        TasksCount = RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.ProjectId == RepositoryFacade.ProjectRepository.Get()
                        .Where(p => p.AuthorId == user.Id)
                        .FirstOrDefault(x => x.CreatedAt == RepositoryFacade.ProjectRepository.Get()
                            .Where(p => p.AuthorId == user.Id)
                            .Max(x => x.CreatedAt))?.Id).Count(),
                        UnfinishedAndCanceledTasksCount = RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.PerformerId == user.Id
                        && (x.State == (int)TaskStates.Started || x.State == (int)TaskStates.Created || x.State == (int)TaskStates.Canceled)).ToList().Count(),
                        LongestTask = RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.PerformerId == user.Id)
                           .First(x => x.FinishedAt - x.CreatedAt ==
                           RepositoryFacade.ProjectTaskRepository.Get()
                           .Where(x => x.PerformerId == user.Id).Max(x => x.FinishedAt - x.CreatedAt))

                    }).First(x => x.User.Id == id);
                }
                throw new ArgumentException("Not found projects and tasks");
            }
            throw new ArgumentException("Not found user");
        }
        public List<ProjectInfoDTO> GetProjectsInfo()
        {

            return RepositoryFacade.ProjectRepository.Get().Where(x => RepositoryFacade.ProjectTaskRepository.Get().Where(t => t.ProjectId == x.Id).Any()).Select(proj => new ProjectInfoDTO
            {
                Project = proj,
                LongestTaskByDicription = RepositoryFacade.ProjectTaskRepository.Get().Where(x=>x.ProjectId == proj.Id)
                          .First(x => x.Description.Length == RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.ProjectId == proj.Id)
                          .Max(x => x.Description.Length)),
                LessTaskByName = RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.ProjectId == proj.Id)
                          .First(x => x.Name.Length == RepositoryFacade.ProjectTaskRepository.Get().Where(x => x.ProjectId == proj.Id)
                          .Min(x => x.Name.Length)),

                UserCounWhereDescriptionMore20orTaskCounLess3 = RepositoryFacade.UserRepository.Get()
                .Where(u => u.TeamId == RepositoryFacade.ProjectRepository.Get().Where(x=>x.Description.Length > 20 || 
                RepositoryFacade.ProjectTaskRepository.Get()
                .Where(t=>t.ProjectId == x.Id).Count() < 3).FirstOrDefault(x=>x.Id == proj.Id).Id).Count()


            }).ToList();

            
        }
    }
}
