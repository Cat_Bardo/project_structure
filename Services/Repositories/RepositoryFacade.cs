﻿using ProjectStructure.Models;
using ProjectStructure.Interfaces;


namespace ProjectStructure.Repositories
{
    public class RepositoryFacade
    {
        public static IRepository<Project> ProjectRepository { get; set; }
        public static IRepository<ProjectTask> ProjectTaskRepository { get; set; }
        public static IRepository<Team> TeamRepository { get; set; }
        public static IRepository<User> UserRepository { get; set; }
        public RepositoryFacade(IRepository<Project> projectRepository,
        IRepository<ProjectTask> projectTaskRepository,
        IRepository<Team> teamRepository,
        IRepository<User> userRepository)
        {
            ProjectRepository = projectRepository;
            ProjectTaskRepository = projectTaskRepository;
            TeamRepository = teamRepository;
            UserRepository = userRepository;
        }

    }
}
