﻿using System;
using System.Collections.Generic;
using System.Linq;

using ProjectStructure.Interfaces;

namespace ProjectStructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        private List<TEntity> entities { get; set; }
        public Repository(List<TEntity> entities)
        {
            this.entities = entities;
        }
        public Repository()
        {
            entities = new List<TEntity>();
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null)
        {
            if(filter != null)
            {
                return entities.Where(filter);
            }
            else
                return entities;
        }
        public TEntity Get(int id)
        {
            if (entities.Any(e => e.Id == id))
                return entities.First(x=>x.Id == id);
            else
                throw new ArgumentException("Entity whis same id not found");
        }
        public void Create(TEntity entity)
        {
            if (entities.Any(e => e.Id == entity.Id))
                throw new ArgumentException("Entity whis same id already exists");
            else
                entities.Add(entity);  
        }
        public void Update(TEntity entity)
        {
            if (entities.Any(e => e.Id == entity.Id))
            {
                int index=0;
                foreach (var item in entities)
                {
                    if (item.Id == entity.Id)
                    {
                        index = entities.IndexOf(item);
                        break;
                    }
                }
                entities.RemoveAt(index);
                entities.Insert(index, (TEntity)entity);
            }
            else
                throw new ArgumentException("Entity whis same id not found");
        }
        public void Delete(int id)
        {
            if (entities.Any(e => e.Id == id))
                entities.Remove(entities.First(e => e.Id == id));
            else
                throw new ArgumentException("Entity whis same id not found");
        }
        public void Delete(TEntity entity)
        {
            if (entities.Any(e => e.Id == entity.Id))
                entities.Remove(entities.First(e => e.Id == entity.Id));
            else
                throw new ArgumentException("Entity whis same id not found");
        }

    }
}
