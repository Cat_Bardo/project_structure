﻿using System;
using System.Collections.Generic;


namespace ProjectStructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> Get(Func<TEntity,bool> filter = null);
        TEntity Get(int id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);
        void Delete(TEntity entity);

    }
}

