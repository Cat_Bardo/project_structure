﻿
namespace ProjectStructure.Interfaces
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
